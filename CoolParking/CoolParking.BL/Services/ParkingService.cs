﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System.Timers;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private List<TransactionInfo> transactions;
        private ILogService LogService;
        private ITimerService TimerWithdraw;
        private ITimerService TimerLog;
      
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            if (withdrawTimer == null || logService == null || logTimer == null)
            {
                throw new InvalidOperationException("Null reference");
            }
            
            Parking p = Parking.GetInstanceParking();
            Parking.Balance = Settings.StartBalance;
            
            transactions = new List<TransactionInfo>();

            TimerWithdraw = withdrawTimer;
            TimerLog = logTimer; 
            LogService = logService;
            
            TimerWithdraw.Interval = Settings.PeriodWithdrawSeconds * 1000; 
            TimerLog.Interval = Settings.PeriodLogSeconds * 1000;

            TimerWithdraw.Elapsed += WithdrawTimer;
            TimerLog.Elapsed += TimerLogService;

            TimerWithdraw.Start();
            TimerLog.Start();
                
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle != null && Settings.Capacity > Parking.Vehicles.Count)
            {
                if (Find(vehicle.Id) != null)
                {
                    throw new ArgumentException("A vehicle with this ID already exists!");
                }
                
                Parking.Vehicles.Add(vehicle);
            }
            else
            {
                throw new InvalidOperationException("Parking lot is full or incorrect vehicle!");
            }
        }

        public void Dispose()
        {
            Parking.Vehicles.Clear();
            transactions.Clear();
            TimerWithdraw.Dispose();
            TimerLog.Dispose();

            if (LogService is LogService)
            {
                ((LogService)LogService).Dispose();
            }
            
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - Parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
        }

        public string ReadFromLog()
        {
            string infoFromFile = "";

            try
            {
                infoFromFile = LogService.Read();
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return infoFromFile;
        }

        public void RemoveVehicle(string vehicleId)
        {
            vehicleId = vehicleId.ToUpper();
            
            Vehicle tmp = Find(vehicleId);

            if (tmp != null)
            {
                if (tmp.Balance < 0)
                {
                    throw new InvalidOperationException("Vehicle balance is below zero!");
                }
                Parking.Vehicles.Remove(tmp);
            }
            else
            {
                throw new ArgumentException("There is no such vehicle!");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            vehicleId = vehicleId.ToUpper();
            Vehicle tmp = Find(vehicleId);

            if (tmp != null && sum >= 0)
            {
                tmp.Balance += sum;
            }
            else
            {
                throw new ArgumentException("There is no such vehicle or vehicle balance is below zero!");
            }
        }

        private Vehicle Find(string vehicleId)
        {
            for (int i = 0; i < Parking.Vehicles.Count; i++)
            {
                if (Parking.Vehicles[i].Id.Equals(vehicleId))
                {
                    return Parking.Vehicles[i];
                }
            }
            return null;
        }

        private void WithdrawTimer(object sender, ElapsedEventArgs e)
        {

            for (int i = 0; i < Parking.Vehicles.Count; i++)
            {
                switch (Parking.Vehicles[i].VehicleType)
                {
                    case VehicleType.Bus: 
                    {
                        WithdrawVenicle(Parking.Vehicles[i], Settings.TariffBus);
                        break;
                    }
                
                    case VehicleType.Truck:
                    {
                        WithdrawVenicle(Parking.Vehicles[i], Settings.TariffTruck);
                        break;
                    }

                    case VehicleType.Motorcycle:
                    {
                        WithdrawVenicle(Parking.Vehicles[i], Settings.TariffMotorcycle);
                        break;
                    }
        
                    case VehicleType.PassengerCar:
                    {
                        WithdrawVenicle(Parking.Vehicles[i], Settings.TariffPassengerCar);
                        break;
                    }
                 }

            }

        }

        private void WithdrawVenicle(Vehicle vehicle, decimal trafic)
        {
            TransactionInfo transaction = new TransactionInfo();

            transaction.IdVehicle = vehicle.Id;
            transaction.TimeTransaction = DateTime.Now;

             if(vehicle.Balance < trafic && vehicle.Balance > 0)
            {
                decimal sumOfWithdraw = vehicle.Balance + (trafic - vehicle.Balance) * Settings.CoefficientFine;
                vehicle.Balance -= sumOfWithdraw;
                transaction.Sum = sumOfWithdraw;
                Parking.Balance += sumOfWithdraw;
            }
            else if (vehicle.Balance >= trafic)
            {
                vehicle.Balance -= trafic;
                transaction.Sum = trafic;
                Parking.Balance += trafic;
            }
            else if(vehicle.Balance <= 0)
            {
                vehicle.Balance -= trafic * Settings.CoefficientFine;
                transaction.Sum = trafic * Settings.CoefficientFine;
                Parking.Balance += trafic * Settings.CoefficientFine;
            }
            
            transactions.Add(transaction);
        }

        private void TimerLogService(object o, ElapsedEventArgs e)
        {

            StringBuilder sendString = new StringBuilder("");

            for (int i = 0; i < transactions.Count; i++)
            {
                sendString.Append(transactions[i].ToString());
            }
            
            try
            {
                LogService.Write(sendString.ToString());
            }
            catch(Exception exeption)
            {
                Console.WriteLine(exeption.Message);
            }
            finally
            {
                transactions.Clear();
            }
        }
    }
}

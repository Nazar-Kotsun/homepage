using System;

namespace CoolParking.BL.Services
{
    public static class CheckVehicleNumbers
    {
        private static bool IsLetters(string strCheck)
        {
            if (strCheck.Length == 2)
            {
                for (int i = 0; i < 2; i++)
                {
                    if (!Char.IsLetter(strCheck[i]))
                    {
                        return false;
                    }
                }
                return true;
            }

            return false;
        }

        private static bool IsNumbers(string strCheck)
        {
            if (strCheck.Length == 4)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (!Char.IsNumber(strCheck[i]))
                    {
                        return false;
                    }
                }
                return true;
            }

            return false;
        }

        public static bool CheckVehicleNumber(string id)
        {
            string[] splitNumbers = id.Split('-');

             if (splitNumbers.Length == 3)
            {
                return IsLetters(splitNumbers[0]) && IsNumbers(splitNumbers[1]) && IsLetters(splitNumbers[2]);
            }

            return false;
        }
    }
}
using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class MainMenu
    {
        private bool Exit = false;
        private int Choose;
        private ParkingService ParkingService;
        private TimerService TimerWithDraw;
        private TimerService TimerLog;
        private ILogService LogService;
        string WorkPath =  $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        
        public MainMenu()
        {
            TimerWithDraw = new TimerService();
            TimerLog = new TimerService();
            LogService = new LogService(WorkPath);

            try
            {
                ParkingService = new ParkingService(TimerWithDraw, TimerLog, LogService);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void StartMenu()
        {
            while (!Exit)
            {
                ShowMenu();
            }
        }

        private void ShowStyleLine(string message)
        {
            Console.WriteLine("-0-*-0-*-0-*-0-*-0-*-" + message + "-*-0-*-0-*-0-*-0-*-0-");
        }

        private void StopProgramToContinue()
        {
            Console.Write("Please enter any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }
        private void CheckCorrectEnteringInteger(out int value)
        {
            while (!int.TryParse(Console.ReadLine(), out value))
            {
                Console.WriteLine("Incorrect entering!");
            }
            
        }

        private void ShowMenu()
        {
            Console.WriteLine("1 - Show current balance of parking");
            Console.WriteLine("2 - Show sum of money for the current period");
            Console.WriteLine("3 - Show count of free places");
            Console.WriteLine("4 - Show all the transactions of parking for the current period");
            Console.WriteLine("5 - Show history of transactions");
            Console.WriteLine("6 - Show list of vehicles");
            Console.WriteLine("7 - Put the vehicle on the parking");
            Console.WriteLine("8 - Pick up vehicle at the parking");
            Console.WriteLine("9 - Replenish the balance of a particular car");
            Console.WriteLine("0 - Exit");
            DoChoice();
        }

        private void DoChoice()
        {
            Console.Write("Your choice: ");
            CheckCorrectEnteringInteger(out Choose);

            switch (Choose)
            {
                case 1:
                {
                    ShowCurrentBalanceOfParking();
                    StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    ShowSumOfMoneyForTheCurrentPeriod();
                    StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    ShowCountOnFreePlaces();
                    StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    ShowLastParkingTransactions();
                    StopProgramToContinue();
                    break;
                }
                case 5:
                {
                    ShowAllTheTransactions();
                    StopProgramToContinue();
                    break;
                }
                case 6:
                {
                    ShowListOfVehicles();
                    StopProgramToContinue();
                    break;
                }
                case 7:
                {
                    AddVehicle();
                    StopProgramToContinue();
                    break;
                }
                case 8:
                {
                    RemoveVehicle();
                    StopProgramToContinue();
                    break;
                }
                case 9:
                {
                    ReplenishTheBalance();
                    StopProgramToContinue();
                    break;
                }
                case 0:
                {
                    ExitTheProgram();
                    break;
                }
            }
            
        }

        private void ShowCurrentBalanceOfParking()
        {
            ShowStyleLine("Current balance");
            Console.WriteLine("Current balance: " + ParkingService.GetBalance());
        }

        private void ShowSumOfMoneyForTheCurrentPeriod()
        {
            ShowStyleLine("Sum of money for the current period");
            
            decimal sumOfMoneyForTheCurrentPeriod = 0;
            
            foreach (var transaction in ParkingService.GetLastParkingTransactions())
            {
                sumOfMoneyForTheCurrentPeriod += transaction.Sum;
            }
            Console.WriteLine("Sum of money for the current period: "  + sumOfMoneyForTheCurrentPeriod);
        }

        private void ShowCountOnFreePlaces()
        {
            ShowStyleLine("Count of free places");
            Console.WriteLine("Free:" + ParkingService.GetFreePlaces());
            Console.WriteLine("Parking capacity:" + ParkingService.GetCapacity());
        }

        private void ShowLastParkingTransactions()
        {
            Console.WriteLine("Number of transactions: " + ParkingService.GetLastParkingTransactions().Length);
            for (int i = 0; i < ParkingService.GetLastParkingTransactions().Length; i++)
            {
                ShowStyleLine(i + "- Transaction");
                Console.WriteLine(ParkingService.GetLastParkingTransactions()[i].ToString());
            }
            ShowStyleLine("");
        }
        
        private void ShowAllTheTransactions()
        {
            ShowStyleLine("All the transactions");
            try
            {
                Console.WriteLine(ParkingService.ReadFromLog());
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void ShowListOfVehicles()
        {
            Console.WriteLine("Number of vehicles: " + ParkingService.GetVehicles().Count);
            
            for (int i = 0; i < ParkingService.GetVehicles().Count; i++)
            {
                ShowStyleLine((i+1) + " - Vehicle");
                Console.WriteLine(ParkingService.GetVehicles()[i]);
            }

            ShowStyleLine("");
        }

        private void AddVehicle()
        {
            int chooseType;
            string vehicleID;
            decimal balance = 0;
            VehicleType vehicleType;
            
            ShowStyleLine("Type vehicle");
            
            Console.WriteLine("Choose type vehicle:");
            Console.WriteLine("1 - PassengerCar");
            Console.WriteLine("2 - Truck");
            Console.WriteLine("3 - Bus");
            Console.WriteLine("4 - Motorcycle");
            CheckCorrectEnteringInteger(out chooseType);

            switch (chooseType)
            {
                case 1:
                {
                    vehicleType = VehicleType.PassengerCar;
                    break;
                }
                case 2:
                {
                    vehicleType = VehicleType.Truck;
                    break;
                }
                case 3:
                {
                    vehicleType = VehicleType.Bus;
                    break;
                }
                case 4:
                {
                    vehicleType = VehicleType.Motorcycle;
                    break;
                }
                default: vehicleType = VehicleType.PassengerCar; break;
            }
            
            ShowStyleLine("Balance");
            
            do
            {
                Console.Write("Enter balance: ");
                
            } while (!decimal.TryParse(Console.ReadLine(), out balance));
            
            ShowStyleLine("ID");
            
          
            Console.Write("Enter id: ");
            vehicleID = Console.ReadLine();
            
            try
            {
                Vehicle vehicle = new Vehicle(vehicleID, vehicleType, balance);
                ParkingService.AddVehicle(vehicle);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void RemoveVehicle()
        {
            ShowStyleLine("Remove Vehicle");
            try
            {
                Console.Write("Enter ID of vehicle which you want to remove: ");
                ParkingService.RemoveVehicle(Console.ReadLine());
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void ReplenishTheBalance()
        {
       
            ShowStyleLine("Top up balance");
            
            try
            {
                decimal balance;
                Console.Write("Enter ID vehicle which you want to top up the balance: ");
                string vehicleId = Console.ReadLine();
                do
                {
                    Console.Write("Enter balance: ");

                } while (!decimal.TryParse(Console.ReadLine(), out balance));
                ParkingService.TopUpVehicle(vehicleId, balance);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void ExitTheProgram()
        {
            Exit = true;
            ParkingService.Dispose();
        }
        
    }
}

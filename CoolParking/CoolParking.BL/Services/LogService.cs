﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.Text;
using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{


    public class LogService : ILogService, IDisposable
    {
        private string Path;

        public LogService(string path)
        {
            Path = path;
        }
         
        public string LogPath
        {
            get
            {
                return Path;
            }
        }

        public string Read()
        {
            string infoFromFile = "";

            try
            {
                using (StreamReader streamReader = new StreamReader(LogPath))
                {
                    infoFromFile = streamReader.ReadToEnd();
                }
            }

            catch(FileNotFoundException fileNotFound)
            {
                string howToCreateFile = "To create file you need to add new vehicle then in 60 sec file will create!";
                throw new InvalidOperationException(fileNotFound.Message + "\n" + howToCreateFile);
            }
            catch (IOException ioExeption)
            {
                Console.WriteLine(ioExeption.Message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return infoFromFile;
        }

        public void Write(string logInfo)
        {
            if (logInfo != "")
            {
                try
                {
                    using (StreamWriter streamWriter = new StreamWriter(LogPath, true))
                    {
                        streamWriter.WriteLine(logInfo);
                    }
                }

                catch (IOException ioExeption)
                {
                    Console.WriteLine(ioExeption.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void Dispose()
        {
            File.Delete(Path);
        }

    }

}

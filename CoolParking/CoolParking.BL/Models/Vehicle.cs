﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using CoolParking.BL.Services;

using System.Text;

namespace CoolParking.BL.Models
{

    public class Vehicle 
    {
        public string Id { get; private set;}
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            id = id.ToUpper();

            if(!CheckVehicleNumbers.CheckVehicleNumber(id) || balance <= 0)
            {
                throw new ArgumentException("Vehicle number entered incorrectly or thr balance below zero!");
            }

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            StringBuilder stringNewID = new StringBuilder(10);
            bool check;
            
            do
            {
                check = false;
                stringNewID = stringNewID.Clear();
                    
                stringNewID.Append(Convert.ToString((char)random.Next(65, 90)));
                stringNewID.Append(Convert.ToString((char)random.Next(65, 90)));
                stringNewID.Append("-");
                stringNewID.Append(Convert.ToString((char)random.Next(48, 57)));
                stringNewID.Append(Convert.ToString((char)random.Next(48, 57)));
                stringNewID.Append(Convert.ToString((char)random.Next(48, 57)));
                stringNewID.Append(Convert.ToString((char)random.Next(48, 57)));
                stringNewID.Append("-");
                stringNewID.Append(Convert.ToString((char)random.Next(65, 90)));
                stringNewID.Append(Convert.ToString((char)random.Next(65, 90)));
                    
                for (int i = 0; i < Parking.Vehicles.Count; i++)
                { 
                    if (Parking.Vehicles[i].Id.Equals(stringNewID.ToString()))
                    { 
                        check = true;
                        break;
                    }
                }
    
            } while (check);
                
            return stringNewID.ToString();
        }
        
        public override string ToString()
        {
            return $"Id: {Id}\nVehicle type: {VehicleType}\nBalance: {Balance}\n";
        }
    }

}
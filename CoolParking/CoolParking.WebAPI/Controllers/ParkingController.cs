using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private ParkingService _parkingService;

        public ParkingController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public IActionResult GetVehicles()
        {
            return Ok( _parkingService.GetVehicles().ToArray());
        }
        
        //GET api/parking/balance
        [HttpGet("balance")]
        public IActionResult GetParkingBalance()
        {
            return Ok(_parkingService.GetBalance());
        }
        
        //GET api/parking/capacity

        [HttpGet("capacity")]
        public IActionResult GetParkingCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }
        
        //GET api/parking/freePlaces

        [HttpGet("freePlaces")]
        public IActionResult GetParkingFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
        
    }
}
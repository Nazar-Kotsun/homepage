using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private ParkingService _parkingService;

        public TransactionsController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        
        // GET api/transactions/last
        [HttpGet("last")]
        public IActionResult GetLastTransactions()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }
        
        //GET api/transactions/all
        [HttpGet("all")]
        public IActionResult GetAllTransactions()
        {
               string message;
               message = _parkingService.ReadFromLog();
               
               if (message == "")
               {
                   return NotFound("Not found the file log");
               }

               return Ok(message);
            //5/10/2020 11:21:20 AM: 3.50 money withdrawn from vehicle with Id='GP-5263-GC'
        }
        
        //PUT api/transactions/topUpVehicle

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicleBalance([FromBody] TransactionService transactionService)
        {
            //If body is invalid - Status Code: 400 Bad Request

            if (!ModelState.IsValid)
            {
                return BadRequest("Model isn't valid");
            }
            
            if (!CheckVehicleNumbers.CheckVehicleNumber(transactionService.Id))
            {
                return BadRequest("The id is bad");
            }

            if (!_parkingService.GetVehicles().Any(vehicle => { return vehicle.Id.Equals(transactionService.Id); }))
            {
                return NotFound("Not found this id");
            }

            try
            {
                _parkingService.TopUpVehicle(transactionService.Id, transactionService.Sum);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Ok(_parkingService.GetVehicles().First(vehicle => vehicle.Id.Equals(transactionService.Id)));
        }
        
    }
}
using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Cors;

namespace CoolParking.WebAPI.Controllers
{
   
    
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private ParkingService _parkingService;

        public VehiclesController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        
        // GET api/vehicles
        [EnableCors("AllowAllOrigin")]
        [HttpGet]
        public IActionResult GetAllVehicles()
        {
            return Ok(_parkingService.GetVehicles());
        }
        
        // GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpGet("{id}")]
        public IActionResult GetVehiclesById(string id)
        {
            if (!CheckVehicleNumbers.CheckVehicleNumber(id))
            {
                return BadRequest("The id is bad");
            }

            if (!_parkingService.GetVehicles().Any(vehicle => { return vehicle.Id.Equals(id); }))
            {
                return NotFound("Not found such the id");
            }
            
            return Ok(_parkingService.GetVehicles().First(vehicle => { return vehicle.Id.Equals(id);}));
        }
        
        // POST api/vehicles
        [HttpPost]
        public IActionResult PostNewVehicle([FromBody] VehicleService vehicleService)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }

            Vehicle vehicle;
            try
            {
                 vehicle = new Vehicle(vehicleService.Id, vehicleService.VehicleType, vehicleService.Balance);
                _parkingService.AddVehicle(vehicle);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest("This vehicle is already existing");
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
                
            return Created("", vehicle);
        }
        
        //DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (!CheckVehicleNumbers.CheckVehicleNumber(id))
            {
                return BadRequest("The id is bad");
            }

            if (!_parkingService.GetVehicles().Any(vehicle => { return vehicle.Id.Equals(id); }))
            {
                return NotFound("Not found such the id");
            }

            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

    }

    
}